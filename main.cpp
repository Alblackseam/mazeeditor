#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "QmlTypes.h"

using namespace std;

int main(int argc, char *argv[]) {
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;
	QmlTypes::registerAll();

	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	//QObject *root = engine.rootObjects()[0];
	//QQuickWindow *window = qobject_cast<QQuickWindow *>(root);
	//assert(window);

	return app.exec();
}
