#pragma once
#include <optional>
#include <QObject>
#include "QmlTypes.h"
#include "PaintedItem.h"
#include "Maze.h"



class Scene : public QObject
{
	Q_OBJECT
public:

public slots:
	void initialize(PaintedItem* paintedItem_);
	void setScaleExponent(int scaleExponent_);
	void redraw();

	void mouseClick(int posx, int posy);
	void wheelEvent(qreal x, qreal y, QPoint angle, int modifers);
	void mousePressed(qreal x, qreal y, int button);
	void mouseReleased(qreal x, qreal y, int button);

	void save();
	void load(QString fileName);
	void saveImage();


	QStringList getMazesFileNames();

private:
	float scale() const;
	float cellHelfSize() const;

	void leftMousePressed(float x, float y) {}
	void leftMouseReleased(float x, float y) {}
	void rightMousePressed(float x, float y);
	void rightMouseReleased(float x, float y);

	int scaleExponent {43};
	QPointF origin {};
	Maze _worldMap{30, 40};
	PaintedItem *_paintedItem{};
	std::optional<QPointF> catchedPoint;

	std::map<Qt::MouseButton, std::function<void(float x, float y)>> mousePressedHandlers = {
		{Qt::MouseButton::LeftButton, [this](float x, float y){ leftMousePressed(x, y); }},
		{Qt::MouseButton::RightButton, [this](float x, float y){ rightMousePressed(x, y); }}
	};

	std::map<Qt::MouseButton, std::function<void(float x, float y)>> mouseReleasedHandlers = {
		{Qt::MouseButton::LeftButton, [this](float x, float y){ leftMouseReleased(x, y); }},
		{Qt::MouseButton::RightButton, [this](float x, float y){ rightMouseReleased(x, y); }}
	};

	static inline auto result = QmlTypes::registerType<Scene>("SceneComponents", 1, 0, "ModelScene");
};
