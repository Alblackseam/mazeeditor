import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.12
import SceneComponents 1.0

Window {
	visible: true
	width: 1000
	height: 700
	title: qsTr("Hello World")

	ModelScene{
		id: modelScene
	}

	StackView {
		id: stackView
		initialItem: mainView
		anchors.fill: parent

		pushEnter: Transition {
			PropertyAnimation {
				property: "opacity"
				from: 0
				to:1
				duration: 200
			}
		}
		pushExit: Transition {
			PropertyAnimation {
				property: "opacity"
				from: 1
				to:0
				duration: 200
			}
		}
		popEnter: Transition {
			PropertyAnimation {
				property: "opacity"
				from: 0
				to:1
				duration: 200
			}
		}
		popExit: Transition {
			PropertyAnimation {
				property: "opacity"
				from: 1
				to:0
				duration: 200
			}
		}
	}

	Component {
		id: mainView
		Scene{
		}
	}

	Component {
		id: loadView
		Load{
		}
	}
}
