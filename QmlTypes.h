#pragma once

#include <string>
#include <memory>
#include <QtQml/qqml.h>
#include <QDebug>


struct QmlTypeDescriptorBase {
	QmlTypeDescriptorBase(const char *uri, int versionMajor, int versionMinor, const char *qmlName)
		: uri{uri}
		, versionMajor{versionMajor}
		, versionMinor{versionMinor}
		, qmlName{qmlName}
	{
	}

	virtual ~QmlTypeDescriptorBase() = default;

	virtual int registerType() = 0;

	const char *uri;
	int versionMajor;
	int versionMinor;
	const char *qmlName;
};


template<typename QmlType>
struct QmlTypeDescriptor : public QmlTypeDescriptorBase {
	using Type = QmlType;

	QmlTypeDescriptor(const char *uri, int versionMajor, int versionMinor, const char *qmlName)
		: QmlTypeDescriptorBase{uri, versionMajor, versionMinor, qmlName}
	{
	}

	int registerType() override {
		qDebug() << "qmlRegisterType" << uri << versionMajor << versionMinor << qmlName;
		return qmlRegisterType<Type>(uri, versionMajor, versionMinor, qmlName);
	}
};



struct QmlTypes {
	static auto& types() {
		static std::map<std::string, std::shared_ptr<QmlTypeDescriptorBase>> typeDescriptors;
		return typeDescriptors;
	}


	static void registerAll() {
		for(auto&& type : types())
			type.second->registerType();
	}

	template<typename QmlType>
	static bool registerType(const char *uri, int versionMajor, int versionMinor, const char *qmlName) {
		//qDebug() << "registerType:" << qmlName << versionMajor << versionMinor;
		types()[std::string(qmlName)] = std::make_shared<QmlTypeDescriptor<QmlType>>(uri, versionMajor, versionMinor, qmlName);
		return true;
	}
};

