#pragma once

#include <iostream>
#include <thread>
#include <cctype>
#include <cstdio>
#include <array>
#include <cassert>
#include <fstream>

#include <QObject>
#include "QmlTypes.h"
#include "PaintedItem.h"




struct Direction {
	uint8_t right : 1;
	uint8_t down : 1;
	uint8_t left : 1;
	uint8_t up : 1;

	int score() const {
		return right + down * 2 + left * 4 + up * 8;
	}
};



struct Maze {
	Maze(int width, int height) : width{width}, height{height}
	{
		resize(width, height);
	}

	void resize(int width_, int height_) {
		rightWall.resize(height_);
		for (auto& walls : rightWall)
			walls.resize(width_ - 1);
		downWall.resize(height_-1);
		for (auto& walls : downWall)
			walls.resize(width_);
		width = width_;
		height = height_;
	}

	bool verticalWall(int x, int y) const {
		if ((x < 0) || (x > width)) return false;
		if ((y < 0) || (y > height)) return false;
		if (y == height) return false;
		if ((x == 0) || (x == width)) return true;
		return rightWall[y][x - 1];
	}

	bool horizontalWall(int x, int y) const {
		if ((x < 0) || (x > width)) return false;
		if ((y < 0) || (y > height)) return false;
		if (x == width) return false;
		if ((y == 0) || (y == height)) return true;
		return downWall[y - 1][x];
	}

	void verticalWallSet(int x, int y, bool enable) {
		if ((x < 0) || (x > width)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if ((y < 0) || (y > height)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if (y == height) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if ((x == 0) || (x == width)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		rightWall[y][x - 1] = enable;
	}

	void horizontalWallSet(int x, int y, bool enable) {
		if ((x < 0) || (x > width)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if ((y < 0) || (y > height)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if (x == width) return; // throw std::out_of_range{"incorrect wall index x, y"};
		if ((y == 0) || (y == height)) return; // throw std::out_of_range{"incorrect wall index x, y"};
		downWall[y - 1][x] = enable;
	}

	Direction cross(int x, int y) const { // x = 0 .. Width
		Direction d;
		d.left = horizontalWall(x - 1, y);
		d.right = horizontalWall(x, y);
		d.up = verticalWall(x, y - 1);
		d.down = verticalWall(x, y);
		return d;
	}

	Direction cell(int x, int y) const { // x = 0 .. Width - 1
		Direction d;
		d.left = verticalWall(x, y);
		d.right = verticalWall(x + 1, y);
		d.up = horizontalWall(x, y);
		d.down = horizontalWall(x, y + 1);
		return d;
	}

	int getWidth() const { return width; }
	int getHeight() const { return height; }

	void save(std::string fileName)
	{
		using namespace std;
		ofstream fl{fileName, ios::binary | ios::out};
		fl.write(reinterpret_cast<char*>(&width), sizeof(width));
		fl.write(reinterpret_cast<char*>(&height), sizeof(height));

		auto writeWalls = [&fl](auto& walls) {
			for (auto row = std::begin(walls); row != std::end(walls); ++row) {
				for (auto wall = std::begin(*row); wall != std::end(*row); ++wall) {
					unsigned char value {*wall};
					fl << value; // static_cast<unsigned char>(*wall);
				}
			}
		};

		writeWalls(rightWall);
		writeWalls(downWall);
	}

	void load(std::string fileName)
	{
		using namespace std;
		ifstream fl{fileName, ios::binary};
		if (!fl.is_open())
			return;

		unsigned int w = 0;
		unsigned int h = 0;
		fl.read(reinterpret_cast<char*>(&w), sizeof(width));
		fl.read(reinterpret_cast<char*>(&h), sizeof(height));
		resize(w, h);

		auto readWalls = [&fl](auto& walls) {
			for (auto row = std::begin(walls); row != std::end(walls); ++row) {
				for (auto wall = std::begin(*row); wall != std::end(*row); ++wall) {
					unsigned char value {0};
					fl >> value;
					*wall = value;
				}
			}
		};

		readWalls(rightWall);
		readWalls(downWall);
	}

protected:
	int width;
	int height;

	std::vector<std::vector<bool>> rightWall{};
	std::vector<std::vector<bool>> downWall{};
};


