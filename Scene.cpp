#include <cmath>
#include <filesystem>
#include "Scene.h"
#include <QPainter>
#include <QPaintDevice>




void Scene::redraw()
{
	using namespace std;
	if (!_paintedItem)
		return;

	_paintedItem->lines.clear();
	auto itemHalfWidth = 0.5f * _paintedItem->width();
	auto itemHalfHeight = 0.5f * _paintedItem->height();

	for (int y = 0; y <= _worldMap.getHeight(); ++y) {
		for (int x = 0; x <= _worldMap.getWidth(); ++x) {
			auto score = _worldMap.cross(x, y).score();
			assert(score < 16);
			//cout << score << ",";
			//cout << (char)crosses[score];
			//cout << (int)_worldMap.verticalWall(x, y) << ",";
			//cout << (int)m.horizontalWall(x, y) << ",";

			auto cell = _worldMap.cell(x, y);

			QPointF center{(x + origin.x()) * scale() + scale() / 2 + itemHalfWidth
						 , (y + origin.y()) * scale() + scale() / 2 + itemHalfHeight};

			auto cellHelfSize = scale() * 0.47f;

			QPointF a(center.x() - cellHelfSize, center.y() - cellHelfSize);
			QPointF b(center.x() + cellHelfSize, center.y() - cellHelfSize);
			QPointF c(center.x() + cellHelfSize, center.y() + cellHelfSize);
			QPointF d(center.x() - cellHelfSize, center.y() + cellHelfSize);


			QColor colorR {u"blue"};;
			QColor colorD{colorR};
			QColor colorL{colorR};
			QColor colorU{colorR};

			//QColor colorR {255,0,0};
			//QColor colorD{0,255,0};
			//QColor colorL{0,0,255};
			//QColor colorU{255,0,0};

			if (cell.right)
				_paintedItem->lines.push_back(PaintedItem::Line{QLineF{b, c}, colorR});
			if (cell.down)
				_paintedItem->lines.push_back(PaintedItem::Line{QLineF{c, d}, colorD});
			if (cell.left)
				_paintedItem->lines.push_back(PaintedItem::Line{QLineF{d, a}, colorL});
			if (cell.up)
				_paintedItem->lines.push_back(PaintedItem::Line{QLineF{a, b}, colorU});
		}
		//cout << endl;
	}

	_paintedItem->update();
}

void Scene::initialize(PaintedItem* paintedItem_)
{
	_paintedItem = paintedItem_;
}

void Scene::setScaleExponent(int scaleExponent_)
{
	scaleExponent = scaleExponent_;
}

void Scene::mouseClick(int posx, int posy)
{
	if (!_paintedItem)
		return;

	auto itemSize = QPointF{ _paintedItem->width(), _paintedItem->height()};

	auto pos = QPointF( posx, posy);
	pos -= 0.5f * itemSize;
	pos -= origin * scale();

	auto cellX = static_cast<int>(pos.x() / scale());
	auto cellY = static_cast<int>(pos.y() / scale());

	auto cellOx = pos.x() - cellX * scale();
	auto cellOy = pos.y() - cellY * scale();

	qDebug() << cellX << "," << cellY << "      " << cellOx << cellOy;


	auto a = cellOx + cellOy - scale();
	auto b = cellOx - cellOy;
	qDebug() << a << " " << b;

	if (a * b > 0){
		qDebug() << "left or right";
		if (a > 0){
			auto value = _worldMap.verticalWall(cellX + 1, cellY);
			qDebug() << _worldMap.verticalWall(cellX + 1, cellY);
			_worldMap.verticalWallSet(cellX + 1, cellY, !value);
		} else {
			auto value = _worldMap.verticalWall(cellX, cellY);
			qDebug() << _worldMap.verticalWall(cellX, cellY);
			_worldMap.verticalWallSet(cellX, cellY, !value);
		}
	} else {
		qDebug() << "up or down";
		if (a > 0){
			auto value = _worldMap.horizontalWall(cellX, cellY + 1);
			qDebug() << _worldMap.horizontalWall(cellX, cellY + 1);
			_worldMap.horizontalWallSet(cellX, cellY + 1, !value);
		} else {
			auto value = _worldMap.horizontalWall(cellX, cellY);
			qDebug() << _worldMap.horizontalWall(cellX, cellY);
			_worldMap.horizontalWallSet(cellX, cellY, !value);
		}
	}

	//_worldMap.cell(cellX, cellY).down = 1; // !_worldMap.cell(cellX, cellY).down;
}

void Scene::wheelEvent(qreal x, qreal y, QPoint angle, int modifers)
{
	int shift = angle.y() / 120;
	scaleExponent += shift;
}

void Scene::mousePressed(qreal x, qreal y, int button)
{
	auto it = mousePressedHandlers.find(static_cast<Qt::MouseButton>(button));
	if (it != std::end(mousePressedHandlers))
		(*it).second(x, y);
}

void Scene::mouseReleased(qreal x, qreal y, int button)
{
	auto it = mouseReleasedHandlers.find(static_cast<Qt::MouseButton>(button));
	if (it != std::end(mouseReleasedHandlers))
		(*it).second(x, y);
}

void Scene::save()
{
	_worldMap.save("maze.bin");
}

void Scene::load(QString fileName)
{
	_worldMap.load(fileName.toStdString());
}

QStringList Scene::getMazesFileNames()
{
	using namespace std::filesystem;
	QStringList result;
	for (const auto & entry : directory_iterator(current_path())) {
		if (!entry.path().extension().empty() && (entry.path().extension() == ".bin")) {
			result.append(QString::fromStdString(entry.path().stem().string() + std::string{".bin"}));
		}
	}

	return result;
}

void Scene::saveImage()
{
	static constexpr auto cellSizePixels = 32;
	QImage image{_worldMap.getWidth() * cellSizePixels, _worldMap.getHeight() * cellSizePixels, QImage::Format_Grayscale8};
	image.fill(QColor(u"white"));

	QPainter painter;
	painter.begin(&image);
	painter.drawRect(0, 0, image.width() - 1,image.height() - 1);

	for (int y = 0; y < _worldMap.getHeight(); ++y) {
		for (int x = 0; x < _worldMap.getWidth(); ++x) {
			auto cell = _worldMap.cell(x, y);

			QPointF center(x * cellSizePixels + cellSizePixels / 2, y * cellSizePixels + cellSizePixels / 2);

			auto cellHelfSize = cellSizePixels / 2;

			QPointF a(center.x() - cellHelfSize, center.y() - cellHelfSize);
			QPointF b(center.x() + cellHelfSize, center.y() - cellHelfSize);
			QPointF c(center.x() + cellHelfSize, center.y() + cellHelfSize);
			QPointF d(center.x() - cellHelfSize, center.y() + cellHelfSize);

			if (cell.right)
				painter.drawLine(b, c);
			if (cell.down)
				painter.drawLine(c, d);
		}
	}
	painter.end();

	image.save("image.png", "PNG");
}





float Scene::scale() const
{
	return powf(1.1f, scaleExponent);
}

float Scene::cellHelfSize() const
{
	return scale() * 0.5f;
}

void Scene::rightMousePressed(float x, float y)
{
	catchedPoint = QPointF{x, y};
}

void Scene::rightMouseReleased(float x, float y)
{
	if (!catchedPoint)
		return;
	auto delta = QPointF{x, y} - *catchedPoint;
	origin += delta / scale();
	catchedPoint.reset();
}



