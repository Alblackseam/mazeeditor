import QtQuick 2.11
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import SceneComponents 1.0


RowLayout {
	id: layout

	ListModel {
		id: fileNames
	}

	anchors {
		fill: parent
		leftMargin: 5
		rightMargin: 5
		topMargin: 5
		bottomMargin: 5
	}
	spacing: 1

	Rectangle {
		color: 'gray'
		Layout.fillWidth: true
		Layout.fillHeight: true
		Layout.minimumWidth: 50
		Layout.minimumHeight: 150

		ListView {
			anchors.fill: parent

			model: fileNames
			delegate: Row {
				Text {
					text: fileName
				}
				Rectangle {
					width: 100
					height: 100

					PaintedItem{
						id: paintedItem
						anchors.fill: parent

						ModelScene{
							id: scenePreview
						}

						Component.onCompleted: {
							scenePreview.initialize(paintedItem)
							scenePreview.setScaleExponent(10)
							scenePreview.redraw()
						}
					}
				}
				Button {
					text: "load"
					onClicked: {
						modelScene.load(fileName)
						modelScene.redraw()
						stackView.pop()
					}
				}
			}
		}

		Component.onCompleted: {
			var fileNameList = modelScene.getMazesFileNames()
			for (var fn in fileNameList)
				fileNames.append({fileName: fileNameList[fn]})
		}
	}

	Column {
		Layout.fillHeight: true
		spacing: 5



		Button {
			text: "Back"

			enabled: stackView.depth > 1

			onClicked: {
				stackView.pop()
				//modelScene.load()
				//modelScene.redraw()
			}
		}
	}
}


