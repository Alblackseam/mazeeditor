import QtQuick 2.11
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import SceneComponents 1.0


RowLayout {
	id: layout
	anchors {
		fill: parent
		leftMargin: 5
		rightMargin: 5
		topMargin: 5
		bottomMargin: 5
	}
	spacing: 1

	Rectangle {
		color: 'lightgray'
		Layout.fillWidth: true
		Layout.fillHeight: true
		Layout.minimumWidth: 50
		Layout.minimumHeight: 150

		PaintedItem{
			id: paintedItem
			anchors.fill: parent

			Component.onCompleted: {
				modelScene.initialize(paintedItem)
				modelScene.redraw()
			}

			MouseArea{
				anchors.fill: parent
				acceptedButtons: Qt.LeftButton | Qt.RightButton

				onClicked: {
					if (mouse.button != Qt.LeftButton)
						return;
					modelScene.mouseClick(mouse.x, mouse.y)
					modelScene.redraw()
				}

				onPressed: {
					modelScene.mousePressed(mouse.x, mouse.y, mouse.button)
				}

				onReleased: {
					modelScene.mouseReleased(mouse.x, mouse.y, mouse.button)
					modelScene.redraw()
				}

				onWheel: {
					modelScene.wheelEvent(wheel.x, wheel.y, wheel.angleDelta, wheel.modifiers)
					modelScene.redraw()
				}
			}
		}
	}

	Column {
		Layout.fillHeight: true
		spacing: 5

		Button {
			text: "Save"
			onClicked: {
				modelScene.save()
				modelScene.redraw()
			}
		}

		Button {
			text: "Load"
			onClicked: {
				stackView.push(loadView)
			}
		}

		Button {
			text: "Image"
			onClicked: {
				modelScene.saveImage()
				modelScene.redraw()
			}
		}
	}
}


