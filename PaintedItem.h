#pragma once

#include <QQuickPaintedItem>
#include "QmlTypes.h"
#include <QPainter>





class PaintedItem : public QQuickPaintedItem{
	Q_OBJECT
public:
	struct Line{
		QLineF place;
		QColor color;
	};

	PaintedItem(QQuickItem *parent = nullptr) : QQuickPaintedItem(parent){
	}

	std::vector<Line> lines;

public slots:


protected:
	void paint(QPainter *painter) override {

		for (auto&& line: lines){
			painter->setPen(line.color);
			painter->drawLine(line.place);
		}
	}

	static inline auto result = QmlTypes::registerType<PaintedItem>("SceneComponents", 1, 0, "PaintedItem");
};
